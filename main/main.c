#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "../lib/fila.h"

void erro() {
    printf("Um erro ocorreu! Finalizando o programa...");
    exit(1);
}

void menu(int *opcao)
{
    puts("=========MENU=========");
    puts("[1] - para adicionar");
    puts("[2] - para remover");
    puts("[3] - para visualizar a lista");
    puts("[4] - para sair");
    puts("======================");
    printf("Digite uma opção: ");
    scanf("%d",opcao); 
}


void adicionar(struct Fila *fila) {
    printf("Digite o número para adicionar na fila: ");
    int num;
    scanf("%d",&num);
    if (!adicionar_elemento(fila, &num)) erro();
}

void remover(struct Fila *fila) {
    int removido; 
    if (!remover_elemento(fila, &removido)) {
        puts("Não há elementos para remover...");
        return;
    }
    printf("O elemento removido foi: %d\n", removido);
}

void visualizar_callback(struct Fila *fila, struct _Elemento *atual, void *data) {
    printf("%d -> ", *(int *)atual->elemento);
    if (atual->proximo == NULL) puts("FIM");
}

void visualizar(struct Fila *fila) {
    if (fila->quantidade_elementos == 0) {
        puts("Não há elementos na lista!");
    }
    iterar(fila, visualizar_callback, NULL);
}

int main(void) {

    bool continuar = true;

    struct Fila *fila = criar_fila(sizeof(int));
    if (!fila) {
        erro();
    }

    while (continuar) {
        int opcao;
        menu(&opcao);
        switch (opcao) {
            case 1:
            adicionar(fila);
            break;
            case 2:
            remover(fila);
            break;
            case 3:
            visualizar(fila);
            break;
            case 4:
            printf("saindo...\n");
            continuar = false;
            break;
            default:
            printf("A opção digitade é inválida! Pressione <ENTER> para continuar...");
        }
    }
}