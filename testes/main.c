#include <assert.h>
#include "../lib/fila.h"

int main(void)
{
    struct Fila *fila = criar_fila(sizeof(int));

    int primeiro = 1;
    int segundo = 2;
    int terceiro = 3;

    adicionar_elemento(fila, &primeiro);
    adicionar_elemento(fila, &segundo);
    adicionar_elemento(fila, &terceiro);

    int elemento_excluido = 0;

    remover_elemento(fila, &elemento_excluido);

    assert(elemento_excluido == primeiro);

    remover_elemento(fila, &elemento_excluido);

    assert(elemento_excluido == segundo);

    remover_elemento(fila, &elemento_excluido);

    assert( elemento_excluido == terceiro);

    deletar_fila(&fila);

    return 0;
}
